import React, { Component } from 'react';
import './productheader.css';
import Headerimage from '../images/header1.jpg'
import Founderimage from '../images/founder.jpg';
import Quote from '../images/chat.png';
import { Link } from 'react-router-dom';
import Man from '../images/men.jpg';
import 'bootstrap/dist/css/bootstrap.min.css'

export default class Header extends Component {
    render() {
        return (
            <div className="contaner  w-100">
                <div className='row mx-0 w-100 no-gutter'>
                    <div className=" col col-12 col-lg-6 order-2 order-lg-1 mx-0" >
                        <div className='d-flex flex-lg-column  justify-content-center  h-100 mx-0 px-3 px-lg-0 py-5 py-lg-0'>
                            <div>
                                <div className='color-set'>Hey ,Welcome to Muse+Meta !</div>
                                <div className='font-size-40'>Discover Trends.</div>
                                <div>
                                    <div className='px-4 border-div'>
                                        <div className='d-flex flex-lg-row '>
                                            <div><img src={Founderimage} className='Founder-image' /></div>
                                            <div className=""><div className='font-weight-bolder'>Jennier Walter</div><div className='font-weight-light text-muted' >Customer</div></div>
                                        </div>
                                        <div className="w-85 py-3"><div><img src={Quote} className='quote' /></div><div className=''>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </div></div>
                                    </div>

                                </div>
                                <div><Link to="/Home" className='home-text-decoration'><button className=''>Home</button></Link></div>
                            </div>
                        </div>
                    </div>
                    <div className='col col-12 col-lg-6 image-section order-1 order-lg-2 mx-0 '><img src={Man} className='header-image ' /></div>

                </div>
            </div>
        )
    }
}
// <img src={Man} className='header-image' />