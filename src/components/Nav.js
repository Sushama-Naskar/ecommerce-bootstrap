import React, { Component } from 'react'
import './nav.css';
import {Link} from 'react-router-dom';
export default class Nav extends Component {
    render() {
        return (
            <div className="nav">
            <div className='logo'>Muse+Meta</div>
            <div className='links'>
                <div><Link to="/" className='text-decoration'>HOME</Link></div>
                <div><Link to="/Product" className='text-decoration'>PRODUCT</Link></div>
                
            </div>
            </div>
        )
    }
}
