import React, { Component } from 'react'
import './about.css'

export default class About extends Component {
    render() {
        return (
            <div className='about'>
            <div className='about-heading'>About Our Products</div>
                <div className='about-text'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus non tincidunt quam. In hac habitasse platea dictumst. Nam in eleifend dolor. Mauris lorem nunc, viverra et ornare id, aliquam id enim. In malesuada facilisis felis, ac placerat justo sodales vel. Maecenas volutpat vehicula magna eget molestie. Sed mollis, neque vel lobortis laoreet, magna lectus vestibulum risus, vel viverra metus velit eu erat. Integer convallis luctus ex, at lobortis quam finibus ac. Proin suscipit sit amet ipsum in consectetur. Maecenas scelerisque, sem eget pretium blandit, elit nulla tempus purus, eu viverra justo purus quis nibh. Aliquam sed accumsan leo, id lobortis ante. Donec luctus aliquet magna, gravida pulvinar nunc. Aliquam non leo felis.</div>
            </div>
        )
    }
}
