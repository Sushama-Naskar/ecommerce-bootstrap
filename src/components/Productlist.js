import React, { Component } from 'react';
import './productlist.css';
import Man from '../images/men.jpg';
import Productheader from './Productheader';
import Star from '../images/star.png';
import 'bootstrap/dist/css/bootstrap.min.css'

export default class Productlist extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        console.log(this.props.product);
        return (
            <div className="productlist w-100">

                <Productheader />
                {
                    (this.props.product) ? <div>

                        <div className=' mt-5 d-flex flex-column justify-content-start pb-5'>
                            {
                                this.props.product.map((element) => {
                                    return (
                                        <div className="card mt-5 box-shadow-lg" key={element.id}>
                                            <div className="d-flex flex-column flex-lg-row justify-content-start ">
                                                <div className='d-flex flex-column justify-content-center align-items-center pt-3 p-lg-3' > <img src={element.image} className='product-image ' /></div>

                                                <div className="d-flex flex-column justify-content-center px-5 py-3">
                                                    <div className='title font-weight-bolder mx-sm-auto mx-lg-0'> {element.title}</div>
                                                    <div className='text-muted font-weight-light pt-3 pb-3'>{element.description}</div>

                                                    <div className='price pb-2'>${element.price}</div>
                                                    <div className='text-muted d-flex flex-row  justify-content-start align-items-center'><div>{element.rating.rate}</div><div className='px-2'><img src={Star} className='star' /></div><div>({element.rating.count})</div></div>

                                                </div>
                                            </div>
                                        </div>
                                    )
                                })
                            }

                        </div>
                    </div> : null

                }

            </div>
        )
    }
}




