import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Cssloader from './components/Cssloader';
import Home from './components/Home';
import Product from './components/Product';
import Nav from './components/Nav';

export default class App extends Component {

  render() {
// console.log(this.state.product);
    return (
      <Router>
      <div className='app'>
      <Nav/>
      <Switch>
      <Route exact path="/Product"> <Product /></Route>
      <Route path="/"><Home/></Route>
      </Switch>
      </div>
     </Router>
    )
  }
}



 